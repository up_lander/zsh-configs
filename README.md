# ZSH configs

This repository contains my custom ZSH configurations, including a custom theme based on `bira` theme and the `.zshrc` file.

## Installation

To use these configurations, follow these steps:

1. Clone the repository:
    ```bash
    git clone https://gitlab.com/up_lander/zsh-configs.git
    ```

2. Copy the `uplander.zsh-theme` file to the appropriate directory:
    ```bash
    cp themes/uplander.zsh-theme ~/.oh-my-zsh/themes/
    ```

3. Copy the `.zshrc` file to your home directory:
    ```bash
    cp .zshrc ~/
    ```

4. Restart your terminal or run `source ~/.zshrc` to apply the changes.

## Theme

The custom theme included in this repository is called `uplander.zsh-theme`.
It provides a personalized look for your ZSH shell. `ZSH_THEME` is set to `uplander` in `.zshrc` file.

Do not forget to install `zsh-autosuggestions` and `zsh-syntax-highlighting`.

Feel free to customize the theme further by modifying the `uplander.zsh-theme` file or creating your own theme based on it.
